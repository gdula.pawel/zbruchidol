(ns zbruchidol.core.templates-test
  (:require [clojure.test :refer :all]
            [selmer.parser :refer [render]]
            [zbruchidol.core.templates :refer :all])
  (:import (zbruchidol.core.file FileAwareDomain)))


; ---------------------------------
; Stubs definition
; ---------------------------------

(defrecord TestDomainWithFileAwareDomain [id logo] FileAwareDomain
  (get_file_prefix [this] "test-domain")
  (get_file_location [this] "/path/to/image/")
  (get_file_field_name [this] :logo))

; ---------------------------------
; Test image tag
; ---------------------------------

(deftest image-tag-empty-parameters-test
  (let [image (render "{% image %}" {})]
    (is (= "" image))))

(deftest image-tag-no-data-in-handler-test
  (let [image (render "{% image company %}" {})]
    (is (= "" image))))

(deftest image-tag-no-image-should-return-empty-string-tests
  (let [image (render "{% image company %}" {:company (map->TestDomainWithFileAwareDomain {:id 666})})]
    (is (= "" image))))

(deftest image-tag-all-ok-test
  (let [image (render "{% image company %}" {:company (map->TestDomainWithFileAwareDomain {:id 666 :logo "image666.jpg"})})]
    (is (= "<img src=\"http://localhost:8080/path/to/image/image666.jpg\" />" image))))

(deftest image-tag-with-css-class-test
  (let [image (render "{% image company class1 class2 %}" {:company (map->TestDomainWithFileAwareDomain {:id 666 :logo "image666.jpg"})})]
    (is (= "<img src=\"http://localhost:8080/path/to/image/image666.jpg\" class=\"class1 class2\" />" image))))
