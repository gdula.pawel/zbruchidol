(ns zbruchidol.catalog.company.db
  (:import (zbruchidol.core.crud PersistencableDomain)
           (zbruchidol.core.file FileAwareDomain)))

(defrecord Company [id name type logo date_created last_updated i18n]

  PersistencableDomain
  (get_table [this] :companies)
  (before_save [this] (assoc this :type "bank"))
  (constrains [this] {:name [[:not-empty true] [:max-size 100]]})
  (from_sql_result [this map] (map->Company map))

  FileAwareDomain
  (get_file_location [this] "/companies/")
  (get_file_prefix [this] "company")
  (get_file_field_name [this] :logo))


