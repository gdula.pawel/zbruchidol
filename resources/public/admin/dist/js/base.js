function executeDelete(link) {
    if (confirm('Are you sure')){
        var $link = $(link);
        var href = $link.attr("href");
        var $antiForgeryField = $('#__anti-forgery-token');
        var antiForgeryId = $antiForgeryField.val();

        $.ajax({
            url: href,
            type: 'DELETE',
            data: {},
            contentType:'application/json',
            dataType: 'text',
            headers: {"X-CSRF-Token":antiForgeryId},
            success: function(result) {window.location=result},
            error: function(result){alert('There was an error while deleting data. Reload page and try again.')}
        });
    }

    return false;
}

$(document).ready(function() {

});