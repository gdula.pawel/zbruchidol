CREATE TABLE i18n (
   id bigint NOT NULL DEFAULT nextval('id_generator'),
   domain_id bigint NOT NULL,
   locale VARCHAR (5) NOT NULL,
   fields json NOT NULL,
   date_created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
   last_updated TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
);

CREATE UNIQUE INDEX i18n_domain_idx ON i18n (domain_id, locale);