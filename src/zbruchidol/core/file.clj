(ns zbruchidol.core.file
  (:require [zbruchidol.core.log :as log]
            [clojure.java.io :as io]
            [zbruchidol.core.config :as conf]
            [clojure.string :refer [split lower-case]]
            [clojure.string :as str])
  (:import (java.util UUID)))

(def logger (log/create-logger "zbruchidol.core.file"))

; ------------------------------------------------------------------------
; Data structures and protocol definition
; ------------------------------------------------------------------------
(defprotocol FileAwareDomain
  (get-file-location [this])
  (get-file-prefix [this])
  (get-file-field-name [this]))

(defrecord
  File [filename content-type tempfile size])

; ------------------------------------------------------------------------
; Domain validation
; ------------------------------------------------------------------------

(defn- validate-domain-protocol [domain]
  (if (not (satisfies? FileAwareDomain domain))
    (throw (ex-info "FileAwareDomain protocol not implemented" {:validation-source domain}))
    domain))

(defn- validate-domain-id [domain]
  (if (nil? (:id domain))
    (throw (ex-info "Id not set" {:validation-source domain}))
    domain))

(defn- validate-domain [domain]
  (some-> domain (validate-domain-protocol) (validate-domain-id)))

; ------------------------------------------------------------------------
; File validation
; ------------------------------------------------------------------------

(defn- validate-file-exists [file]
  (if (and (not (nil? file))
           (not= (:size file) 0)
           (not (nil? (:tempfile file))))
    file))

(defn- validate-file-size [file]
  (if (> (:size file) (:file-max-size (conf/file-upload-config)))
    (throw (ex-info "File is to big" {:validation-source file}))
    file))

(defn- validate-file-format [file]
  (if (not (some #(= % (:content-type file)) (:file-content-type (conf/file-upload-config))))
    (throw (ex-info "Not supported file format" {:validation-source file}))
    file))

(defn- validate-file [file]
  (some-> file validate-file-exists validate-file-size validate-file-format))

; ------------------------------------------------------------------------
; File saving
; ------------------------------------------------------------------------

(defn- get-file-name
  ([domain]
   ((get-file-field-name domain) domain))

  ([domain file]
   (let [unique-part (UUID/randomUUID)
         file-extension (-> (:filename file) (split #"\.") (last) (lower-case))]
     (str (get-file-prefix domain) "-" (:id domain) "-" unique-part "." file-extension))))

(defn- save-file [destination-path original-file]
  (with-open [in (io/input-stream original-file)
              out (io/output-stream (io/file destination-path))]
    (io/copy in out)))

; ------------------------------------------------------------------------
; Interfaces definition
; ------------------------------------------------------------------------

(defn valid? [file]
  (validate-file-exists file))

(defn store! [domain file]
  (do
    (logger :info "Initialize file storage for" domain "with id" (:id domain))
    (if (and (validate-domain domain) (validate-file file))
      (let [file-name (get-file-name domain file)
            file-storage-path (:file-storage-path (conf/file-upload-config))
            file-domain-path (str (get-file-location domain) file-name)
            file-location (str file-storage-path file-domain-path)
            tempfile (:tempfile file)]
        (logger :info "Storing file" file "under path" file-location)
        (save-file file-location tempfile)
        (assoc domain (get-file-field-name domain) file-name))
      domain)))

(defn delete! [domain]
  (do
    (logger :info "Initialize file deletion for" domain "with id" (:id domain))
    (if (and (validate-domain domain))
      (let [file-name (get-file-name domain)
            file-storage-path (:file-storage-path (conf/file-upload-config))
            file-domain-path (str (get-file-location domain) file-name)
            file-location (str file-storage-path file-domain-path)]
        (if (not (nil? file-name))
          (try
            (do
              (logger :info "File to be removed is under the path" file-location)
              (io/delete-file file-location)
              (logger :info "Setting file name from" file-location "to nil")
              (assoc domain (get-file-field-name domain) nil))
            (catch Exception e
              domain))
          domain)))))