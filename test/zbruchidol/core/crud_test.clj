(ns zbruchidol.core.crud-test
  (:require [clojure.test :refer :all]
            [clojure.string :as string]
            [zbruchidol.core.db :as db]
            [zbruchidol.core.crud :refer [before-save-handler get! get-all! update! save!]])
  (:import (zbruchidol.core.crud PersistencableDomain)
           (clojure.lang ExceptionInfo)))

; ---------------------------------
; Stubs definition
; ---------------------------------

(defrecord TestNotPersistencableDomain [id name type date_created last_updated i18n])

(defrecord TestPersistencableDomain [id name type date_created last_updated i18n]
  PersistencableDomain
  (get_table [this] :test_domain)
  (before_save [this] (assoc this :type "TestType"))
  (from_sql_result [this map] (map->TestPersistencableDomain map))
  (constrains [this] {:name [[:not-empty] [:max-size 10]]}))

; ---------------------------------
; Fixtures definition
; ---------------------------------

(defn create-table []
  (db/create-table :test_domain
                   [[:id "bigint NOT NULL DEFAULT nextval('id_generator')"]
                    [:name "varchar(100)"]
                    [:type "varchar(100)"]
                    [:date_created "TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()"]
                    [:last_updated "TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()"]]))

(defn drop-table []
  (db/drop-table :test_domain))

(defn fixture [test]
  (create-table)
  (test)
  (drop-table))

(use-fixtures :once fixture)

; ---------------------------------
; Utils
; ---------------------------------

(defn insert-test-data [name type]
  (db/insert :test_domain {:name name :type type}))

(defn get-test-data [id]
  (first (db/select ["select * from test_domain where id = ?" id])))

(defn protocol? [domain]
  "Fix for satisfies? NPE issue"
  (let [pattern (re-pattern ".*TestPersistencableDomain.*")
        domain-as-string (str domain)]
    (boolean (re-matches pattern domain-as-string))))

; ---------------------------------
; Tests
; ---------------------------------

(deftest before-save-triggered-for-insert
  (let [domain (before-save-handler (map->TestPersistencableDomain {:name "Foo"}))]
    (is (= "Foo" (:name domain)))
    (is (= "TestType" (:type domain)))
    (is (instance? org.joda.time.DateTime (:last_updated domain)))
    (is (instance? org.joda.time.DateTime (:date_created domain)))))

(deftest before-save-triggered-for-update
  (let [domain (before-save-handler (map->TestPersistencableDomain {:id 666 :name "Foo"}))]
    (is (= "Foo" (:name domain)))
    (is (= "TestType" (:type domain)))
    (is (instance? org.joda.time.DateTime (:last_updated domain)))
    (is (nil? (:date_created domain)))))

; ---------------------------------

(deftest get!-should-expect-correct-interface
  (let [domain (map->TestNotPersistencableDomain {:id 666})]
    (is (thrown-with-msg? RuntimeException #"PersistencableDomain protocol not implemented" (get! domain)))))

(deftest get!-should-return-nil-when-data-not-found
  (let [domain (map->TestPersistencableDomain {:id 666})]
    (is (nil? (get! domain)))))

(deftest get!-should-return-protocol-passed-as-a-parapmeter
  (let [raw-data (insert-test-data "Foo" "Bar")
        domain (get! (map->TestPersistencableDomain {:id (:id raw-data)}))]
    (is (not (nil? domain)))
    (is (protocol? domain))
    (is (not (nil? (re-matches #"\d+" (str (:id domain))))))
    (is (= "Foo" (:name domain)))
    (is (= "Bar" (:type domain)))
    (is (instance? org.joda.time.DateTime (:date_created domain)))
    (is (instance? org.joda.time.DateTime (:last_updated domain)))))

; ---------------------------------

(deftest get-all!-should-expect-correct-interface
  (let [domain (map->TestNotPersistencableDomain {})]
    (is (thrown-with-msg? RuntimeException #"PersistencableDomain protocol not implemented" (get-all! domain)))))

(deftest get-all!-should-return-protocol-passed-as-a-parapmeter
  (let [raw-data-1 (insert-test-data "Foo1" "Bar")
        raw-data-2 (insert-test-data "Foo2" "Bar")
        domains (get-all! (map->TestPersistencableDomain {}))
        domain-1 (first (filter #(= (:id %) (:id raw-data-1)) domains))
        domain-2 (first (filter #(= (:id %) (:id raw-data-2)) domains))]
    (is (not (nil? domain-1)))
    (is (protocol? domain-1))
    (is (not (nil? domain-2)))
    (is (protocol? domain-2))))

; ---------------------------------

(deftest update!-should-expect-correct-interface
  (let [domain (map->TestNotPersistencableDomain {:id 666})]
    (is (thrown-with-msg? RuntimeException #"PersistencableDomain protocol not implemented" (update! domain)))))

(deftest update!-should-trigger-db-update
  (let [raw-data (insert-test-data "Foo" "Bar")
        domain (update! (map->TestPersistencableDomain {:id (:id raw-data), :name "FooX"}))
        raw-reloaded-data (get-test-data (:id raw-data))]
    (is (protocol? domain))
    (is (= (:id raw-reloaded-data) (:id domain)))
    (is (= "Foo" (:name raw-data)))
    (is (= "FooX" (:name raw-reloaded-data)))))

(deftest update!-should-return-protocol-passed-as-a-parapmeter
  (let [raw-data (insert-test-data "Foo" "Bar")
        domain (update! (map->TestPersistencableDomain {:id (:id raw-data), :name "FooX"}))]
    (is (protocol? domain))))

; ---------------------------------

(deftest validation-should-throw-an-ex-info-exception
  (let [domain (map->TestPersistencableDomain {:id 666})]
    (is (thrown-with-msg? ExceptionInfo #"Validation errors" (save! domain)))))

(deftest validation-of-empty-constraint-should-throw-an-ex-info-exception
  (let [domain (map->TestPersistencableDomain {:id 666})]
    (try
      (save! domain)
      (catch RuntimeException e
        (is (instance? ExceptionInfo e))
        (is (= (:validation-errors (ex-data e)) '("Field ':name' can't be empty")))))))

(deftest validation-of-max-size-constraint-should-throw-an-ex-info-exception
  (let [domain (map->TestPersistencableDomain {:id 666 :name "1234567890ABCD"})]
    (try
      (save! domain)
      (catch RuntimeException e
        (is (instance? ExceptionInfo e))
        (is (= (:validation-errors (ex-data e)) '("Field ':name' is to big (max: 10)")))))))

; ---------------------------------

(deftest save!-should-expect-correct-interface
  (let [domain (map->TestNotPersistencableDomain {:id 666 :name "Test"})]
    (is (thrown-with-msg? RuntimeException #"PersistencableDomain protocol not implemented" (save! domain)))))


(deftest save!-should-store-data-correctly
  (let [saved-domain (-> (map->TestPersistencableDomain {:name "Test 1"})
                         (save!))
        reloaded-domain (get-test-data (:id saved-domain))]
    (is (protocol? saved-domain))
    (is (not (nil? (re-matches #"\d+" (str (:id saved-domain))))))
    (is (= (:name saved-domain) (:name reloaded-domain)))))





