(ns zbruchidol.core.file-test
  (:require [clojure.test :refer :all]
            [zbruchidol.core.file :refer [store! delete!]]
            [clojure.java.io :as io])
  (:import (zbruchidol.core.file FileAwareDomain)))

; ---------------------------------
; Stubs definition
; ---------------------------------
(defrecord TestDomainNoFileAwareDomainProtocol [id name logo])

(defrecord TestDomainWithFileAwareDomain [id logo] FileAwareDomain
  (get_file_prefix [this] "test-domain")
  (get_file_location [this] "/")
  (get_file_field_name [this] :logo))

; ---------------------------------
; Utils
; ---------------------------------
(defn get-test-file
  ([] (get-test-file "jpg" 1000))
  ([format size]
   (let [file (io/file (str "./resources/test/image." format))]
     {:filename     (str "logo." format)
      :content-type (str "image/" format)
      :tempfile     file
      :size         size})))

(defn get-empty-test-file []
  {:filename     "test"
   :content-type "application/octet-stream"
   :tempfile     (io/file "tmp/empty")
   :size         0})

(defn file-exists [domain]
  (let [file-path (str "./resources/test/" (:logo domain))
        file (io/file file-path)]
    (.exists file)))

(defn clean-up [domain]
  (try
    (let [file-path (str "./resources/test/" (:logo domain))]
      (io/delete-file file-path))
    (catch Throwable e)))

; ---------------------------------
; store!
; ---------------------------------
(deftest store!-should-not-accept-records-without-implemented-protocol
  (let [domain (map->TestDomainNoFileAwareDomainProtocol {})]
    (is (thrown-with-msg? RuntimeException #"FileAwareDomain protocol not implemented" (store! domain nil)))))


(deftest store!-when-id-doesnt-exist-should-throw-an-exception
  (let [domain (map->TestDomainWithFileAwareDomain {})]
    (is (thrown-with-msg? RuntimeException #"Id not set" (store! domain nil)))))

(deftest store!-when-file-doesnt-exist-should-skip-processing
  (let [domain (map->TestDomainWithFileAwareDomain {:id 666})]
    (is (= (store! domain nil) domain))))

(deftest store!-when-empty-file-uploaded-should-skip-processing
  (let [domain (map->TestDomainWithFileAwareDomain {:id 666})
        file (get-empty-test-file)]
    (is (= (store! domain file) domain))))

(deftest store!-should-not-accept-to-big-files
  (let [file (get-test-file "jpg" 10000)
        domain (map->TestDomainWithFileAwareDomain {:id 666})]
    (is (thrown-with-msg? RuntimeException #"File is to big" (store! domain file)))))

(deftest store!-should-not-accept-not-supported-formats
  (let [file (get-test-file "svg" 10)
        domain (map->TestDomainWithFileAwareDomain {:id 666})]
    (is (thrown-with-msg? RuntimeException #"Not supported file format" (store! domain file)))))

(deftest store!-when-jpg-file-exists-should-save-it-and-override-the-name
  (let [file (get-test-file)
        domain (store! (map->TestDomainWithFileAwareDomain {:id 666}) file)]
    (is (not (nil? (re-matches #"test-domain-666-.*.jpg" (:logo domain)))))
    (is (true? (file-exists domain)))
    (clean-up domain)))

(deftest store!-when-png-file-exists-should-save-it-and-override-the-name
  (let [file (get-test-file "png" 10)
        domain (store! (map->TestDomainWithFileAwareDomain {:id 666}) file)]
    (is (not (nil? (re-matches #"test-domain-666-.*.png" (:logo domain)))))
    (is (true? (file-exists domain)))
    (clean-up domain)))

; ---------------------------------
; delete!
; ---------------------------------

(deftest delete!-should-not-accept-records-without-implemented-protocol
  (let [domain (map->TestDomainNoFileAwareDomainProtocol {})]
    (is (thrown-with-msg? RuntimeException #"FileAwareDomain protocol not implemented" (delete! domain)))))

(deftest delete!-should-skip-processing-when-file-doesnt-exist
  (let [domain (map->TestDomainWithFileAwareDomain {:id 666})]
    (is (= (delete! domain) domain))))

(deftest delete!-when-png-file-exists-should-save-it-and-override-the-name
  (let [file (get-test-file "png" 10)
        domain (store! (map->TestDomainWithFileAwareDomain {:id 666}) file)]
    (is (not (nil? (re-matches #"test-domain-666-.*.png" (:logo domain)))))
    (is (true? (file-exists domain)))

    (let [domain-after-delete (delete! domain)]
      (is (false? (file-exists domain)))
      (is (nil? (:logo domain-after-delete))))))
