(ns zbruchidol.core.templates
  (:require [selmer.parser :as selmer]
            [selmer.util :refer [without-escaping]]
            [zbruchidol.core.security :as sec]
            [zbruchidol.core.file :as file]
            [zbruchidol.core.config :as config]
            [clojure.string :refer [join blank?]]))

; ----------------------------
; Generic
; ----------------------------
(defn text
  ([file] (text file {}))
  ([file params] (selmer/render-file (str "templates/" file) params)))

(defn html
  ([file]
   (html file {}))
  ([file params]
   (without-escaping
     (selmer/render-file (str "templates/" file) params))))

(defn render
  [text params]
   (without-escaping
     (selmer/render text params)))

; ----------------------------
; With predefined admin meta information (eg currenly logged editor)
; ----------------------------
(defn html+meta
  ([file] (html+meta file {}))
  ([file params]
   (let [editor (sec/get-authorized-editor)]
     (html file (merge params {:editor editor})))))

; ----------------------------
; Custom tags
; ----------------------------

(selmer/add-tag! :image
                 (fn [args context-map]
                   (if (and (not (empty? args)) (not (empty? context-map)))
                     (let [key (keyword (first args))
                           css-classes (join " " (rest args))
                           css-statement (if (not (blank? css-classes)) (str "class=\"" css-classes "\" ") "")
                           domain (key context-map)
                           file-url (:file-url (config/file-upload-config))
                           file-path (file/get-file-location domain)
                           file-name ((file/get-file-field-name domain) domain)]
                       (if file-name
                         (str "<img src=\"" file-url file-path file-name "\" " css-statement "/>")
                         ""))
                     "")))
