(defproject zbruchidol "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [compojure "1.6.1"]                           ; web
                 [ring/ring-defaults "0.3.2"]
                 [org.postgresql/postgresql "9.4-1201-jdbc41"] ; sql
                 [com.mchange/c3p0 "0.9.5.5"]
                 [cheshire "5.10.0"]
                 [ragtime "0.8.0"]                             ; migrations
                 [environ "1.1.0"]                             ; configuration
                 [ch.qos.logback/logback-classic "1.1.1"]      ; logging
                 [selmer "1.12.18"]                            ; templating
                 [com.cemerick/friend "0.2.3"]]                ; security
  :plugins [[lein-ring "0.12.5"]
            [lein-environ "1.1.0"]
            [lein-eftest "0.5.9"]]
  :ring {:handler zbruchidol.handler/app}
  :aliases {"migrate"  ["run" "-m" "zbruchidol.core.db/migrate"]
            "rollback" ["run" "-m" "zbruchidol.core.db/rollback"]}
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring/ring-mock "0.3.2"]]}})
