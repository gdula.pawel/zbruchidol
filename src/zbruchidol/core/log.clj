(ns zbruchidol.core.log
  (:require [clojure.string :as string])
  (:import [org.slf4j LoggerFactory]))

(defn create-logger [name]
  (let [logger (LoggerFactory/getLogger name)
        executors {:debug #(.debug logger (print-str %))
                   :info  #(.info logger (print-str %))
                   :warn  #(.warn logger (print-str %))}]
    (fn [level & message] ((executors level) (string/join " "  (map str message))))))