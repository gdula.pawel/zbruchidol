(ns zbruchidol.catalog.country.admin
  (:require [compojure.core :refer :all]
            [zbruchidol.core.templates :as tmpl]
            [zbruchidol.core.security :as sec]
            [zbruchidol.core.i18n :refer [get-default-locale]]
            [zbruchidol.catalog.country.db :as countries]))

(def country-admin-handlers
  [; -------------------------------------
   ; Countries - list
   ; -------------------------------------
   (GET "/countries" []
     (sec/authorize #{:editor}
                    (tmpl/html+meta "admin/countries/list.html"
                                    {:countries (countries/find-all-countries (get-default-locale))})))])