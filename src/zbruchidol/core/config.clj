(ns zbruchidol.core.config
  (:require [environ.core :refer [env]]))

(defn conf [conf-key]
  (env conf-key))

(defn file-upload-config []
  {:file-max-size     (Long/valueOf (conf :file-max-size))
   :file-content-type ["image/jpg" "image/jpeg" "image/png"]
   :file-storage-path (conf :file-storage-path)
   :file-url (conf :file-url)})

(defn development? []
  (= (conf :mode) "DEV"))

(defn db-spec []
  {:classname   (conf :db-classname)
   :subprotocol (conf :db-subprotocol)
   :subname     (conf :db-subname)
   :user        (conf :db-user)
   :password    (conf :db-password)})

(defn db-connection-string []
  (str (str "jdbc:" (conf :db-subprotocol)
            ":" (conf :db-subname)
            "?user=" (conf :db-user)
            "&password=" (conf :db-password))))