(ns zbruchidol.core.db
  (:import (com.mchange.v2.c3p0 ComboPooledDataSource)
           (org.postgresql.util PGobject))
  (:require [ragtime.jdbc :as ragtime]
            [clj-time.coerce :as time-coerce]
            [clojure.java.jdbc :as jdbc]
            [ragtime.repl :as repl]
            [zbruchidol.core.config :as conf]
            [cheshire.core :as json]))

; ----------------------------
; DB initialization
; ----------------------------
(defn- pool
  [spec]
  (let [cpds (doto (ComboPooledDataSource.)
               (.setDriverClass (:classname spec))
               (.setJdbcUrl (str "jdbc:" (:subprotocol spec) ":" (:subname spec)))
               (.setUser (:user spec))
               (.setPassword (:password spec))
               ;; expire excess connections after 30 minutes of inactivity:
               (.setMaxIdleTimeExcessConnections (* 30 60))
               ;; expire connections after 3 hours of inactivity:
               (.setMaxIdleTime (* 3 60 60)))]
    {:datasource cpds}))

(def ^:private pooled-db (delay (pool (conf/db-spec))))
(defn- db-connection [] @pooled-db)

; ----------------------------
; JSON support initiation
; ----------------------------
(defn value-to-json-pgobject [value]
  (doto (PGobject.)
    ;; hack for now -- eventually we should properly determine the actual type
    (.setType "jsonb")
    (.setValue (json/generate-string value))))

(extend-protocol jdbc/ISQLValue
  clojure.lang.IPersistentMap
  (sql-value [value] (value-to-json-pgobject value)))

(extend-protocol jdbc/IResultSetReadColumn
  org.postgresql.util.PGobject
  (result-set-read-column [pgobj metadata idx]
    (let [type (.getType pgobj)
          value (.getValue pgobj)]
      (if (#{"jsonb" "json"} type)
        (json/parse-string value true)
        value))))

; ----------------------------
; Time converting
; ----------------------------
; http://clojure.github.io/java.jdbc/#clojure.java.jdbc/IResultSetReadColumn
; This is coming in from the database
(extend-protocol jdbc/IResultSetReadColumn
  java.sql.Timestamp
  (result-set-read-column [col _ _]
    (time-coerce/from-sql-time col)))

; http://clojure.github.io/java.jdbc/#clojure.java.jdbc/ISQLValue
; This is going back out to the database
(extend-protocol jdbc/ISQLValue
  org.joda.time.DateTime
  (sql-value [v]
    (time-coerce/to-sql-time v)))

; ----------------------------
; SQL interface
; ----------------------------

(defn select [sql-params]
  (jdbc/query (db-connection) sql-params))

(defn insert [table row]
  (let [result (jdbc/insert! (db-connection) table row)]
    (first result)))

(defn insert-all [table rows]
  (jdbc/insert-multi! (db-connection) table rows))

(defn update [table set-map where-clause]
  (let [result (jdbc/update! (db-connection) table set-map where-clause)]
    (boolean result)))

(defn delete
  ([table] (delete table {}))
  ([table where-clause] (jdbc/delete! (db-connection) table where-clause)))

(defn create-table [table-name table-definition]
  (jdbc/db-do-commands (db-connection)
                       (jdbc/create-table-ddl table-name
                                              table-definition)))
(defn drop-table [table-name]
  (jdbc/db-do-commands (db-connection)
                       (jdbc/drop-table-ddl table-name)))

; ----------------------------
; Time utils
; ----------------------------

(defn now []
  (new org.joda.time.DateTime))

; ----------------------------
; DB migration
; ----------------------------

(defn- load-config []
  {:datastore  (ragtime/sql-database (conf/db-connection-string))
   :migrations (ragtime/load-resources "migrations")})

(defn migrate []
  (repl/migrate (load-config)))

(defn rollback []
  (repl/rollback (load-config)))
