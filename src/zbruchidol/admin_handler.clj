(ns zbruchidol.admin-handler
  (:require [compojure.core :refer :all]
            [cemerick.friend :as friend]
            [ring.util.response :refer [redirect]]
            [ring.util.anti-forgery :refer [anti-forgery-field]]
            [zbruchidol.core.templates :as tmpl]
            [zbruchidol.core.security :as sec]
            [zbruchidol.catalog.country.admin :as catalog-country]
            [zbruchidol.catalog.company.admin :as catalog-company]
            [zbruchidol.content.pages.admin :as content-page]))


; -------------------------------------
; Security
; -------------------------------------
(def ^:private security-handlers
  [(GET "/login" [login_failed auth-status]
     (if (friend/current-authentication)
       (redirect "/admin")
       (tmpl/html "admin/default/login.html"
                  {:anti-forgery-field (anti-forgery-field)
                   :login-failed       login_failed
                   :auth-status        auth-status})))

   (friend/logout (ANY "/logout" request (redirect "/admin/login")))])

; -------------------------------------
; Dashboard
; -------------------------------------
(def ^:private dashboard-handlers
  [(GET "/" []
     (sec/authorize #{:editor}
                    (tmpl/html "admin/default/dashboard.html")))])

; -------------------------------------
; All admin routes
; -------------------------------------
(def admin-routes
  (apply routes (concat security-handlers
                        dashboard-handlers
                        catalog-country/country-admin-handlers
                        catalog-company/company-admin-handlers
                        content-page/page-admin-handlers)))