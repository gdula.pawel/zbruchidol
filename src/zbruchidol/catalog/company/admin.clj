(ns zbruchidol.catalog.company.admin
  (:use ring.middleware.session)
  (:require [compojure.core :refer :all]
            [zbruchidol.core.templates :as tmpl]
            [zbruchidol.core.security :as sec]
            [zbruchidol.core.crud :as domain]
            [zbruchidol.core.file :as file]
            [zbruchidol.core.i18n :as i18n]
            [zbruchidol.core.log :as log]
            [zbruchidol.core.i18n :refer [get-default-locale get-supported-locales]]
            [zbruchidol.catalog.company.db :as company]
            [ring.util.anti-forgery :refer [anti-forgery-field]]
            [ring.util.response :refer [redirect]])
  (:import (clojure.lang ExceptionInfo)))

(def logger (log/create-logger "zbruchidol.catalog.company.admin"))

(def company-admin-handlers
  [; -------------------------------------
   ; List
   ; -------------------------------------
   (GET "/companies" [status]
     (sec/authorize #{:editor}
                    (tmpl/html+meta "admin/companies/list.html"
                                    {:anti-forgery-field (anti-forgery-field)
                                     :status             status
                                     :companies          (-> (company/map->Company {})
                                                             (domain/get-all!))})))
   ; -------------------------------------
   ; Add new / form
   ; -------------------------------------
   (GET "/company" []
     (sec/authorize #{:editor}
                    (tmpl/html+meta "admin/companies/create.html"
                                    {:anti-forgery-field (anti-forgery-field)})))
   ; -------------------------------------
   ; Add new / save
   ; -------------------------------------
   (POST "/company" [name]
     (sec/authorize #{:editor}
                    (let [company (company/map->Company {:name name})]
                      (try
                        (let [domain (domain/save! company)]
                          (redirect (str "/admin/company/" (:id domain) "?status=created")))
                        (catch ExceptionInfo e
                          (tmpl/html+meta "admin/companies/create.html"
                                          {:anti-forgery-field (anti-forgery-field)
                                           :status             "error"
                                           :error-message      (.getMessage e)
                                           :validation-errors  (:validation-errors (ex-data e))}))))))

   ; -------------------------------------
   ; Update / form
   ; -------------------------------------
   (GET "/company/:id" [id status]
     (sec/authorize #{:editor}
                    (let [company (-> (company/map->Company {:id id})
                                      (domain/get!))]
                      (tmpl/html+meta "admin/companies/update.html"
                                      {:anti-forgery-field (anti-forgery-field)
                                       :locales            (get-supported-locales)
                                       :company            company
                                       :status             status}))))
   ; -------------------------------------
   ; Update / save
   ; -------------------------------------
   (POST "/company/:id" [id name logo & rest]
     (sec/authorize #{:editor}
                    (let [company (-> (company/map->Company {:id id})
                                      (domain/get!)
                                      (assoc :name name))]
                      (try
                        (do
                          (if (file/valid? logo)
                            (-> company (file/delete!) (file/store! logo) (domain/update!) (i18n/save! rest))
                            (-> company (domain/update!) (i18n/save! rest)))
                          (redirect (str "/admin/company/" id "?status=updated")))
                        (catch RuntimeException e
                          (tmpl/html+meta "admin/companies/update.html"
                                          {:anti-forgery-field (anti-forgery-field)
                                           :locales            (get-supported-locales)
                                           :company            company
                                           :status             "error"
                                           :error-message      (.getMessage e)}))))))
   ; -------------------------------------
   ; Delete
   ; -------------------------------------
   (DELETE "/company/:id" [id]
     (sec/authorize #{:editor}
                    (do
                      (-> (company/map->Company {:id id}) (domain/get!) (file/delete!) (domain/delete!))
                      (str "/admin/companies?status=deleted"))))])