(ns zbruchidol.content.pages.admin
  (:use ring.middleware.session)
  (:require [compojure.core :refer :all]
            [zbruchidol.core.templates :as tmpl]
            [zbruchidol.core.security :as sec]
            [zbruchidol.core.log :as log]
            [ring.util.anti-forgery :refer [anti-forgery-field]]
            [ring.util.response :refer [redirect]]))

(def logger (log/create-logger "zbruchidol.content.pages.admin"))

(def page-admin-handlers
  [; -------------------------------------
   ; List
   ; -------------------------------------
   (GET "/pages" []
     (sec/authorize #{:editor}
                    (tmpl/html+meta "admin/pages/list.html"
                                    {:anti-forgery-field (anti-forgery-field)})))

   ; -------------------------------------
   ; Add new / form
   ; -------------------------------------
   (GET "/page" []
     (sec/authorize #{:editor}
                    (tmpl/html+meta "admin/pages/create.html"
                                    {:anti-forgery-field (anti-forgery-field)})))

   (GET "/page/preview" [id]
     (sec/authorize #{:editor}
                    (tmpl/render "Hello from empty page with id = {{id}}" {:id 666})))])