(ns zbruchidol.core.i18n-test
  (:require [clojure.test :refer :all]
            [zbruchidol.core.db :as db]
            [zbruchidol.core.i18n :refer [i18n save!]]))


(defn my-test-fixture [f]
  (do
    (db/delete :i18n)
    (db/insert-all :i18n [{:domain_id 1 :locale "pl_PL" :fields {:foo "foo data 1" :bar "bar data 1"}}
                          {:domain_id 2 :locale "pl_PL" :fields {:foo "foo data 2" :bar "bar data 2"}}]))
  (f)
  (db/delete :i18n))

(use-fixtures :once my-test-fixture)

; ---------------------------------
; i18n
; ---------------------------------

(deftest i18n-wrong-id
  (is (= (i18n [{:id 666 :email "test@test.pl"}] "pl_PL")
         [{:id 666 :email "test@test.pl"}])))

(deftest i18n-wrong-locale
  (is (= (i18n [{:id 1 :email "test@test.pl"}] "de_DE")
         [{:id 1 :email "test@test.pl"}])))

(deftest i18n-correct-one
  (is (= (i18n [{:id 1 :email "test@test.pl"}] "pl_PL")
         [{:id 1 :email "test@test.pl" :i18n {:foo "foo data 1" :bar "bar data 1"}}])))

(deftest i18n-correct-many
  (is (= (i18n [{:id 1 :email "test@test.pl"} {:id 2 :email "test@test.pl"}] "pl_PL")
         [{:id 1 :email "test@test.pl" :i18n {:foo "foo data 1" :bar "bar data 1"}}
          {:id 2 :email "test@test.pl" :i18n {:foo "foo data 2" :bar "bar data 2"}}])))

; ---------------------------------
; save!
; ---------------------------------


;execute
(deftest save!-no-domain-id-should-throw-an-exception
  (is (thrown-with-msg? RuntimeException #"Parameter domain-id can't be null" (save! nil nil))))

(deftest save!-no-params-should-do-nothing
  (let [save-result (save! {:id 1} [])]
    (is (= (i18n [{:id 1 :email "test@test.pl"}] "pl_PL")
           [{:id 1 :email "test@test.pl" :i18n {:foo "foo data 1" :bar "bar data 1"}}]))))

(deftest save!-correct-data-should-save
  (let [save-result (save! {:id 1001} {"i18n.pl_PL.name" "name-pl"
                                       "i18n.pl_PL.description" "description-pl"
                                       "i18n.uk_UA.name" "name-ua"
                                       "i18n.uk_UA.description" "description-ua"})]

    (is (= [{:id 1001 :email "test@test.pl" :i18n {:name "name-pl" :description "description-pl"}}]
           (i18n [{:id 1001 :email "test@test.pl"}] "pl_PL")))

    (is (= [{:id 1001 :email "test@test.pl" :i18n {:name "name-ua" :description "description-ua"}}]
           (i18n [{:id 1001 :email "test@test.pl"}] "uk_UA")))))


(deftest save!-correct-data-should-save-after-time-and-updated-after-subsequent-calls
  (let [save-result (save! {:id 1002} {"i18n.pl_PL.name" "name-pl" "i18n.pl_PL.description" "description-pl"})
        saved-i18n (i18n [{:id 1002 :email "test@test.pl"}] "pl_PL")
        update-result (save! {:id 1002} {"i18n.pl_PL.name" "name-pl-updated" "i18n.pl_PL.description" "description-pl-updated"})
        updated-i18n (i18n [{:id 1002 :email "test@test.pl"}] "pl_PL")]
    (is (= [{:id 1002 :email "test@test.pl" :i18n {:name "name-pl" :description "description-pl"}}] saved-i18n))
    (is (= [{:id 1002 :email "test@test.pl" :i18n {:name "name-pl-updated" :description "description-pl-updated"}}] updated-i18n))))
