(ns zbruchidol.domains.editors
  (:require [zbruchidol.core.db :as db]
            [clojure.string :as str]))

(defn find-editors-by-email [email]
  (if (not (str/blank? email))
    (db/select ["select * from editors where email = ?" email])))

