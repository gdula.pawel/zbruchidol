{:dev  {:env {:db-classname   "org.postgresql.Driver"
              :db-subprotocol "postgresql"
              :db-subname     "//localhost/zbruch_idol"
              :db-user        "postgres"
              :db-password    "mysecretpassword"
              :test-editor    "test@test.pl"
              :mode           "DEV"                         ; set to "DEV" disables eg authentication and provides test@test.pl editor as identity
              :file-max-size  1000000
              :file-storage-path  "/home/gdulus/Workspace/projects/zbruchidol-static"
              :file-url "http://localhost:8080"}}



 :test {:env {:db-classname   "org.postgresql.Driver"
              :db-subprotocol "postgresql"
              :db-subname     "//localhost/zbruch_idol_test"
              :db-user        "postgres"
              :db-password    "mysecretpassword"
              :test-editor    "test@test.pl"
              :mode           "DEV"
              :file-max-size  1000
              :file-storage-path  "./resources/test"}}}