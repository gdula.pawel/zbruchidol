(ns zbruchidol.core.crud
  (:require [zbruchidol.core.db :as db]
            [zbruchidol.core.i18n :refer [i18n]]
            [zbruchidol.core.log :as log]))

(def logger (log/create-logger "zbruchidol.core.domain"))

; ------------------------------------------------------------------------
; Protocol definition
; ------------------------------------------------------------------------
(defprotocol PersistencableDomain
  (get-table [this])
  (before-save [this])
  (constrains [this])
  (from-sql-result [this map]))

; ------------------------------------------------------------------------
; id converter
; ------------------------------------------------------------------------

(defn- id [domain]
  (Long/valueOf (:id domain)))

; ------------------------------------------------------------------------
; before save actions for create and update
; ------------------------------------------------------------------------

(defn- before-save-dispatcher [domain]
  (if (:id domain) ::update ::insert))

(defn- prepare-domain [domain]
  (-> (before-save domain)
      (dissoc :id :i18n :date_created)
      (assoc :last_updated (db/now))))

(defmulti before-save-handler before-save-dispatcher)
(defmethod before-save-handler ::insert [domain] (-> domain (prepare-domain) (assoc :date_created (db/now))))
(defmethod before-save-handler ::update [domain] (-> domain (prepare-domain)))

; ------------------------------------------------------------------------
; Constraints validation
; ------------------------------------------------------------------------

(defn- not-empty! [field value constrain-params]
  (if (empty? value) (str "Field '" field "' can't be empty")))

(defn- max-size! [field value constrain-params]
  (if (>= (count value) constrain-params) (str "Field '" field "' is to big (max: " constrain-params ")")))

(def ^:private validation-rules
  {:not-empty not-empty!
   :max-size  max-size!})

(defn- validate-field-constrains
  ([domain field constrains]
   (validate-field-constrains domain field constrains []))
  ([domain field constrains errors]
   (if (empty? constrains)
     errors
     (let [[constrain-name constrain-params] (first constrains)
           validation-function (validation-rules constrain-name)
           field-value (field domain)
           error (validation-function field field-value constrain-params)]
       (recur domain field (rest constrains) (remove nil? (conj errors error)))))))

(defn- validate-constrains
  ([domain]
   (validate-constrains domain (constrains domain) []))
  ([domain constrains errors]
   (if (empty? constrains)
     (if (empty? errors)
       domain
       (throw (ex-info "Validation errors" {:validation-source domain :validation-errors errors})))
     (let [[field-name field-constrains] (first constrains)
           field-errors (validate-field-constrains domain field-name field-constrains)]
       (recur domain (rest constrains) (concat errors field-errors))))))

; ------------------------------------------------------------------------
; Basic domain validation
; ------------------------------------------------------------------------

(defn- validate-domain [domain]
  (if (not (satisfies? PersistencableDomain domain))
    (throw (ex-info "PersistencableDomain protocol not implemented" {:validation-source domain}))
    domain))

; ------------------------------------------------------------------------
; Interfaces definition
; ------------------------------------------------------------------------

(defn get!
  ([domain]
   (get! domain i18n))
  ([domain i18n-fn]
   (if (validate-domain domain)
     (let [id (id domain)
           table (name (get-table domain))
           query (str "select * from " table " where id = ?")
           result (db/select [query id])]
       (logger :info "Query '" query "' executed")
       (if (not-empty result)
         (let [result+i18n (i18n-fn result "pl_PL")]
           (from-sql-result domain (first result+i18n))))))))

(defn get-all! [domain]
  (if (validate-domain domain)
    (let [table (name (get-table domain))
          query (str "select * from " table)
          result (db/select [query])
          result+i18n (i18n result "pl_PL")]
      (logger :info "Query '" query "' executed")
      (map #(from-sql-result domain %) result+i18n))))

(defn save! [domain]
  (if (and (validate-domain domain) (validate-constrains domain))
    (let [table (get-table domain)
          map (before-save-handler domain)]
      (logger :info "Inserting" map "to" table)
      (from-sql-result domain (db/insert table map)))))

(defn update! [domain]
  (if (and (validate-domain domain) (validate-constrains domain))
    (let [id (id domain)
          table (get-table domain)
          map (before-save-handler domain)]
      (logger :info "Updating" table "with id" id "with new data" map)
      (if (db/update table map ["id = ?" id])
        (get! domain)))))

(defn delete! [domain]
  (let [id (id domain)
        table (get-table domain)]
    (logger :info "Deleting " table "with id =" id)
    (db/delete table ["id = ?" id])))