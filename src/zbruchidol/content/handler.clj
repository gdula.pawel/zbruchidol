(ns zbruchidol.content.handler
  (:require [compojure.core :refer :all]))

(def content-routes
  (routes
    (GET "/*" [] "Content Served")))
