(ns zbruchidol.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [zbruchidol.core.log :as log]
            [ring.util.response :refer [redirect]]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [zbruchidol.content.handler :as content]
            [zbruchidol.admin-handler :as admin]
            [zbruchidol.core.security :as security]
            [zbruchidol.core.templates :as tmpl]
            [cemerick.friend :as friend]
            (cemerick.friend [workflows :as workflows])))

(def ^:private log (log/create-logger "zbruchidol.handler"))

(defn- redirect+ [request status]
  (do
    (log :info "Redirecting to default page" request)
    (redirect (str "/admin/login?auth-status=" status))))

(defroutes app-routes
           (context "/admin" [] admin/admin-routes)
           (context "/content" [] content/content-routes)
           (route/not-found (tmpl/html "not-found.html" {}))
           (route/resources "/"))
(def app
  (->
    app-routes
    (friend/authenticate {:default-landing-uri     "/admin/"
                          :login-uri               "/admin/login"
                          :unauthenticated-handler #(redirect+ % "unauthenticated")
                          :unauthorized-handler    #(redirect+ % "unauthorized")
                          :credential-fn           security/authorize-editor
                          :workflows               [(workflows/interactive-form)]})
    (wrap-defaults site-defaults)))

