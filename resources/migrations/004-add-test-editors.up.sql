insert into editors
(email, password, first_name, last_name, date_created, last_updated)
values
('test@test.pl', '$2a$10$zjAd9qMXBrfYFXDOnfccc.wG3jMGTM3qMOiQvoLAUQh4odUrxWKny', 'test', 'test', now(), now());