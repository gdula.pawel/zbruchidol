CREATE TABLE editors(
   id bigint NOT NULL DEFAULT nextval('id_generator'),
   email VARCHAR (355) UNIQUE NOT NULL,
   password VARCHAR (100) NOT NULL,
   first_name VARCHAR (100) NOT NULL,
   last_name VARCHAR (100) NOT NULL,
   date_created TIMESTAMP WITH TIME ZONE NOT NULL,
   last_updated TIMESTAMP WITH TIME ZONE NOT NULL
);