(ns zbruchidol.catalog.country.db
  (:require [zbruchidol.core.db :as db]
            [zbruchidol.core.i18n :refer [i18n]]))

(defrecord Country [id name iso_code date_created last_updated i18n])

(defn find-all-countries [locale]
  (let [countries (db/select ["select * from countries"])
        countries-18n (i18n countries locale)]
    (map map->Country countries-18n)))