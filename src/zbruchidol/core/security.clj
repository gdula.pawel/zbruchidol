(ns zbruchidol.core.security
  (:require [zbruchidol.core.log :as log]
            [zbruchidol.core.config :as conf]
            [cemerick.friend :as friend]
            [zbruchidol.domains.editors :as editors]
            (cemerick.friend [credentials :as creds])))

(def log (log/create-logger "zbruchidol.core.security"))

; --------------------------------------------
; Friend wrapper
; --------------------------------------------

(defmacro authorize [roles body]
  `(if (conf/development?)
     (do
       (log :info "DEV mode selected - skipping security check")
       ~body)
     (friend/authorize ~roles ~body)))

; --------------------------------------------
; Editor credentials
; --------------------------------------------
(defn- find-authorized-editor []
  (if (conf/development?)
    (let [editor (conf/conf :test-editor)]
      (log :info "DEV mode selected - returning test editor")
      (editors/find-editors-by-email editor))
    (editors/find-editors-by-email (:identity (friend/current-authentication)))))

(defn get-authorized-editor []
  (let [editor (first (find-authorized-editor))]
    (log :info "Editor found:" (:first_name editor) (:last_name editor))
    editor))

; --------------------------------------------
; Admin authentication
; --------------------------------------------

(defn- bcrypt-verify+ [password hash]
  (if (and password hash)
    (try
      (creds/bcrypt-verify password hash)
      (catch Throwable e false))))

(defn authorize-editor [{:keys [username password]}]
  (let [editor (first (editors/find-editors-by-email username))
        authorized? (boolean (bcrypt-verify+ password (:password editor)))]
    (log :info "User" username "authentication status is" authorized?)
    (if authorized?
      {:identity username :roles #{:editor}})))


; --------------------------------------------
;
; --------------------------------------------