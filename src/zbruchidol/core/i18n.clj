(ns zbruchidol.core.i18n
  (:require [zbruchidol.core.db :as db]
            [zbruchidol.core.log :as log]
            [clojure.string :as str]))

; -------------------------------------------------------

(def logger (log/create-logger "zbruchidol.catalog.company.admin"))

; -------------------------------------------------------

(defn- find-translation [domain-id locale]
  (first (db/select ["select id, fields as i18n from i18n where domain_id = ? and locale = ?" domain-id locale])))

(defn- validate-params [domain data]
  (if (nil? (:id domain)) (throw (ex-info "Parameter domain-id can't be null" {}))))

(defn- filter-i18n-keys [[k v]]
  (and (str/starts-with? k "i18n") (not (str/blank? v))))

(defn- convert-to-map [[k v]]
  (let [elements (str/split k #"\.")]
    {:locale (nth elements 1) :value {(keyword (nth elements 2)) v}}))

(defn- merge-with-db-data-and-save [domain-id [locale data]]
  (let [db-data (find-translation domain-id locale)
        db-data-id (:id db-data)
        db-data-json-object (:i18n db-data)
        requested-data (reduce #(conj %1 (%2 :value)) {} data)
        merged-data (merge db-data-json-object requested-data)]
    (if (nil? db-data-id)
      #(do
         (logger :info "Inserting i18n data" merged-data "for domain" domain-id "and locale" locale)
         (db/insert :i18n {:domain_id domain-id :locale locale :fields merged-data}))
      #(do
         (logger :info "Updating i18n data" merged-data "for domain" domain-id)
         (db/update :i18n {:fields merged-data :last_updated (db/now)} ["id = ?" db-data-id])))))

; -------------------------------------------------------
; PUBLIC INTERFACE
; -------------------------------------------------------

(defn save! [domain data]
  (do
    (validate-params domain data)
    (let [queries (->> data
                       (filter filter-i18n-keys)
                       (map convert-to-map)
                       (group-by :locale)
                       (map #(merge-with-db-data-and-save (:id domain) %)))]
      ; threading above return lazyseq which evaluation need to be forced
      (doseq [query queries] (query)))))

(defn i18n
  ([domains locale]
   (i18n domains locale []))
  ([domains locale domains-i18n]
   (if (empty? domains)
     domains-i18n
     (let [domain (first domains)
           domain-id (:id domain)
           ; id of the translation need to be removed to not override id of the domain
           translation (dissoc (find-translation domain-id locale) :id)
           domain-i18n (merge domain translation)]
       (recur (rest domains) locale (conj domains-i18n domain-i18n))))))

(defn get-supported-locales []
  [{:iso_code "pl_PL" :name "Polish"} {:iso_code "uk_UA" :name "Ukrainian"}])

(defn get-default-locale []
  "pl_PL")
