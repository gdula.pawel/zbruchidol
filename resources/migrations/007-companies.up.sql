CREATE TABLE companies (
     id bigint NOT NULL DEFAULT nextval('id_generator'),
     name VARCHAR (100) NOT NULL,
     type VARCHAR (50) NOT NULL,
     logo VARCHAR (100),
     date_created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
     last_updated TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
 );