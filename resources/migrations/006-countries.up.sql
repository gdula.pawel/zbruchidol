CREATE TABLE countries (
     id bigint NOT NULL DEFAULT nextval('id_generator'),
     name VARCHAR (100) NOT NULL,
     iso_code VARCHAR (2) UNIQUE NOT NULL,
     date_created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
     last_updated TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
 );


INSERT INTO countries (name, iso_code) VALUES ('Germany', 'de');
INSERT INTO i18n
(domain_id, locale, fields)
VALUES
((select id from countries where iso_code = 'de'), 'pl_PL', '{"name": "Niemcy"}');
INSERT INTO i18n
(domain_id, locale, fields)
VALUES
((select id from countries where iso_code = 'de'), 'uk_UA', '{"name": "Німеччина"}');

-- ------------------------------------------------------------

INSERT INTO countries (name, iso_code) VALUES ('Poland', 'pl');
INSERT INTO i18n
(domain_id, locale, fields)
VALUES
((select id from countries where iso_code = 'pl'), 'pl_PL', '{"name": "Polska"}');
INSERT INTO i18n
(domain_id, locale, fields)
VALUES
((select id from countries where iso_code = 'pl'), 'uk_UA', '{"name": "Польща"}');

-- ------------------------------------------------------------

INSERT INTO countries (name, iso_code) VALUES ('Switzerland', 'ch');
INSERT INTO i18n
(domain_id, locale, fields)
VALUES
((select id from countries where iso_code = 'ch'), 'pl_PL', '{"name": "Szwajcaria"}');
INSERT INTO i18n
(domain_id, locale, fields)
VALUES
((select id from countries where iso_code = 'ch'), 'uk_UA', '{"name": "Швейцарія"}');